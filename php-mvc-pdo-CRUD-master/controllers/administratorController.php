<?php
/*
	CRUD creado por Oscar Amado
	Contacto: oscarfamado@gmail.com
*/
class administratorController extends Administrator{

	function index(){
		require_once('views/all/header.php');
		require_once('views/all/nav.php');
		require_once('views/index/index.php');
		require_once('views/index/modals.php');
		require_once('views/all/footer.php');
	}

	function table_users(){
		?>
		<table class="table table-bordered">
			<thead>
				<tr>
				<th>#</th>
				<th>Number</th>
				<th>Check in</th>
				<th>Check out</th>
				<th>Persons</th>
                <th>Small place</th>
                <th>Big place</th>
				</tr>
			</thead>
			<tbody >		
		<?php
		foreach (parent::get_view_users()	as $data) {
		?>
		<tr>
			<td><?php echo $data->id_user; ?> </td>
			<td><?php echo $data->number_user; ?> </td>
			<td><?php echo $data->start_date_user; ?> </td>
			<td><?php echo $data->end_date_user; ?> </td>
            <td><?php echo $data->persons_user; ?> </td>
            <td><?php echo $data->small_place_user; ?> </td>
            <td><?php echo $data->big_place_user; ?> </td>
			<td>
			  <div class="btn-group">
			    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
			    Selecteer <span class="caret"></span></button>
			    <ul class="dropdown-menu" role="menu">
			      <li><a href="#" onclick="ModalUpdate('<?php echo $data->id_user; ?>','<?php echo $data->number_user; ?>','<?php echo $data->start_date_user; ?>','<?php echo $data->end_date_user; ?> ','<?php echo $data->persons_user; ?> ', '<?php echo $data->small_place_user; ?> ','<?php echo $data->big_place_user; ?> ');">Updaten</a></li>
			      <li><a href="#" onclick="deleteUser('<?php echo $data->id_user; ?>');">Verwijderen</a></li>
			    </ul>
			  </div>
			</td>
		</tr>
		<?php
		}
		?>
			</tbody>
		</table>	
	<?php	
    }
    
	function deleteuser(){		
			parent::set_delete_user($_REQUEST['id']);		
    }

	function registeruser(){
		$data = array(
					'number' 		=> $_REQUEST['number'],
					'start_date' => $_REQUEST['start_date'],
					'end_date'		=> $_REQUEST['end_date'],
                    'persons' 		=> $_REQUEST['persons'],
                    'small_place' => $_REQUEST['small_place'],
                    'big_place'		=> $_REQUEST['big_place']
					);		
					parent::set_register_user($data);		
    }    
    
	function updateuser(){
		$data = array(
					'id'		=> $_REQUEST['id'],
                    'number' 		=> $_REQUEST['number'],
                    'start_date' => $_REQUEST['start_date'],
                    'end_date'		=> $_REQUEST['end_date'],
                    'persons' 		=> $_REQUEST['persons'],
                    'small_place' => $_REQUEST['small_place'],
                    'big_place'		=> $_REQUEST['big_place']
					);		
			parent::set_update_user($data);		
	}    
    
}

