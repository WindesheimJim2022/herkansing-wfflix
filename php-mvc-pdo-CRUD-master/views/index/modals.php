<div id="addUser" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reservering maken</h4>
      </div>
      <div class="modal-body">
		<form name="formUser" onsubmit="register(); return false">
		  <div class="input-group">
		    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		    <input id="number" type="text" class="form-control" name="number" placeholder="Number" required autocomplete="off">
		  </div>
		  <br>
		  <div class="input-group">
		    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		    <input id="start_date" type="text" class="form-control" name="start_date" placeholder="Check in" required autocomplete="off">
		  </div>
		  <br>
		  <div class="input-group">
		    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
		    <input id="end_date" type="text" class="form-control" name="end_date" placeholder="Check out" required autocomplete="off">
		  </div>
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="persons" type="text" class="form-control" name="persons" placeholder="Persons" required autocomplete="off">
            </div>
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="small_place" type="text" class="form-control" name="small_place" placeholder="Small place" required autocomplete="off">
            </div>
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                <input id="big_place" type="text" class="form-control" name="big_place" placeholder="Big place" required autocomplete="off">
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Aanmaken</button>
		<button type="button" class="btn btn-danger" data-dismiss="modal">Terug</button>
		</form>
      </div>
    </div>
  </div>
</div>


<div id="updateUser" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reserverig wijzigen </h4>
      </div>
      <div class="modal-body">
		<form name="formUserUpdate" onsubmit="update(); return false">
		<input type="text" name="id" id="id" style="display: none;">
		  <div class="input-group">
		    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		    <input id="name" type="text" class="form-control" name="name" placeholder="Nombres" required autocomplete="off">
		  </div>
		  <br>
		  <div class="input-group">
		    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		    <input id="last_name" type="text" class="form-control" name="last_name" placeholder="Apellidos" required autocomplete="off">
		  </div>
		  <br>
		  <div class="input-group">
		    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
		    <input id="email" type="text" class="form-control" name="email" placeholder="Correo electronico" required autocomplete="off">
		  </div>
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="persons" type="text" class="form-control" name="persons" placeholder="Persons" required autocomplete="off">
            </div>
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="small_place" type="text" class="form-control" name="small_place" placeholder="Small place" required autocomplete="off">
            </div>
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                <input id="big_place" type="text" class="form-control" name="big_place" placeholder="Big place" required autocomplete="off">
            </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Update</button>
		<button type="button" class="btn btn-danger" data-dismiss="modal">Ga terug</button>
		</form>
      </div>
    </div>
  </div>
</div>