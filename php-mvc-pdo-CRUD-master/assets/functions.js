function objectAjax(){
	var xmlhttp = false;
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e){
		try{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");			
		} catch (E){
			xmlhttp = false;	
		}		
	}
	if(!xmlhttp && typeof XMLHttpRequest!='undefined'){
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}
//Inicializo automaticamente la funcion read al entrar a la pagina o recargar la pagina;
addEventListener('load', read, false);

function read(){
        $.ajax({        
        		type: 'POST',
                url:   '?c=administrator&m=table_users',               
                beforeSend: function () {
                        $("#information").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#information").html(response);
                }
        });
}

function register(){
	number 		= document.formUser.number.value;
	start_date 	= document.formUser.start_date.value;
	end_date		= document.formUser.end_date.value;
	persons 		= document.formUser.persons.value;
	small_place 	= document.formUser.small_place.value;
	big_place 		= document.formUser.big_place.value;
	ajax = objectAjax();
	ajax.open("POST", "?c=administrator&m=registeruser", true);
	ajax.onreadystatechange=function() {
		if(ajax.readyState==4){			
			read();			
			alert('Los datos guardaron correctamente.');			
			$('#addUser').modal('hide');
		}
	}
ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
ajax.send("number="+number+"&start_date="+start_date+"&end_date="+end_date+"&persons="+persons+"&small_place="+small_place+"&big_place="+big_place);
}	

function update(){
	id 			= document.formUserUpdate.id.value;
	number 		= document.formUser.number.value;
	start_date 	= document.formUser.start_date.value;
	end_date		= document.formUser.end_date.value;
	persons 		= document.formUser.persons.value;
	small_place 	= document.formUser.small_place.value;
	big_place 		= document.formUser.big_place.value
	ajax = objectAjax();
	ajax.open("POST", "?c=administrator&m=updateuser", true);
	ajax.onreadystatechange=function() {
		if(ajax.readyState==4){
			read();
			$('#updateUser').modal('hide');
		}
	}
ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
ajax.send("number="+number+"&start_date="+start_date+"&end_date="+end_date+"&persons="+persons+"&small_place="+small_place+"&big_place="+big_place+"&id="+id);

}

function deleteUser(id){	
	if(confirm('¿Esta seguro de eliminar este registro?')){						
	ajax = objectAjax();
	ajax.open("POST", "?c=administrator&m=deleteuser", true);
	ajax.onreadystatechange=function() {
		if(ajax.readyState==4){			
			read();		
		}
	}
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("id="+id);
	}
}

function ModalRegister(){
	$('#addUser').modal('show');
}

function ModalUpdate(id,number,start_date,end_date,persons,small_place,big_place){
	document.formUserUpdate.id.value 			= id;
	document.formUserUpdate.number.value 		= number;
	document.formUserUpdate.start_date.value 	= start_date;
	document.formUserUpdate.end_date.value 		= end_date;
	document.formUserUpdate.persons.value 		= persons;
	document.formUserUpdate.small_place.value 	= small_place;
	document.formUserUpdate.big_place.value 	= big_place;
	$('#updateUser').modal('show');
}

/*
	CRUD creado por Oscar Amado
	Contacto: oscarfamado@gmail.com
*/